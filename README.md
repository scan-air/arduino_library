# Scan'Air Arduino Library

A library that makes building air control project a breeze.

## Purpose

As the air quality becomes more and more important, this library can help you build your homemade air quality sensor and connect it to the Scan'Air system.

## Contributing

Please check the [CONTRIBUTING.md](https://gitlab.com/scan-air/arduino_library/-/blob/master/CONTRIBUTING.md) doc for contribution guidelines.

I don't know where to write this but this library use other library to work so here's a list:
 - [ArduinoJson](https://arduinojson.org/).
 - [TinyGPS library](https://github.com/neosarchizo/TinyGPS)
 - [Adafruit BME680 library](https://github.com/adafruit/Adafruit_BME680).
 - [Arduino Wire library](https://www.arduino.cc/en/reference/wire)
 - [Arduino SPI library](https://www.arduino.cc/en/reference/SPI)
 - [Arduino WiFi library](https://www.arduino.cc/en/Reference/WiFi)
 - [Arduino SoftwareSerial library](https://www.arduino.cc/en/Reference/SoftwareSerial)

## License

This library is licensed under [MIT](https://gitlab.com/scan-air/arduino_library/-/blob/master/LICENSE).

## Install

Download the project as a ZIP file and copy/paste it in your Arduino library's folder.
Please check the [official documentation](https://www.arduino.cc/en/Guide/Libraries).

## Online Documentation

You can checkout the online documentation [here](http://x2022scanair3705567454000.francecentral.cloudapp.azure.com:8001/).

## Use

Just include the ScanAirLib.h header and you're ready to go.

```cpp
#include <ScanAirLib.h>
```

Create an instance variable to start doing something. 

```cpp
#include <ScanAirLib.h>

ScanAir instance;
```

### Connect to your network

This library gives you `int ScanAir::connect(const char[] ssid, const char[] password, const char[] secret, const char[] name)` function to connect your homemade sensor to your network and our Scan'Air system.
To use this feature, you will need to create a secret on your Scan'Air Profile and the name of your sensor written the same as in your ScanAir sensor dashboard.

```cpp
#include <ScanAirLib.h>

ScanAir instance;

void setup() {
  // put your setup code here, to run once:
  int status = instance.connect("ssid", "wifipwd", "secret", "sensor's name");
}
```

The variable `status` can be one of those :
```cpp
#define ERROR_CONNECT_WIFI -1
#define ERROR_CONNECT_SERVER 1
#define SUCCESS_CONNECT 0
```

### Get your data

After creating an instance, you can create instance of all supported sensors.

```cpp
#include <ScanAirLib.h>
#include <MP503/MP503.h>
#include <MQ135/MQ135.h>
#include <BME680/BME680.h>
#include <PPD42NS/PPD42NS.h>

ScanAir instance;

// MP503
MP503 sensor = instance.create<MP503>(pin);
// MQ135
MQ135 sensor = instance.create<MQ135>(pin);
// BME680
BME60 sensor = instance.create<BME680>();
// PPD42NS
PPD42NS sensor = instance.create<PPD42NS>(pin, sampletime);
```

Each of these sensors have their own functions to get their data but they follow a simple life cycle described in the [ISensor.h](https://gitlab.com/scan-air/arduino_library/-/blob/master/src/ISensor.h) header.

`int setup()` to setup you sensor in the Arduino `void setup() {}` function.

`int compute()` to compute you sensor data in the Arduino `void loop() {}` function.

If any **get** function is called before those functions, the behaviour is then undefined.

### Send your data to the Scan'Air server

After connecting successfully your Scan'Air instance, each sensors have the `int sendDataToServer()` to send all the data they computed directly to our server.

If you want to send specific data to our server you can use the `send` function from your Scan'Air instance described below : 
```cpp
template<typename T>
int ScanAir::send(const char[] ref, const char[] name, const char[] unit, T data);
```
or
```cpp
int ScanAir::send(const char[] ref, DynamicJsonDocument &json);
```

## Contributors

[Paul Beauduc](https://gitlab.com/acerlorion)

[Nejma Belkhanfar](https://gitlab.com/nejma.B)
