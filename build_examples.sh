#!/bin/bash

examples=(
    "./examples/BareMinimum"
    "./examples/BME680_basic"
    "./examples/MG811_basic"
    "./examples/MP503_basic"
    "./examples/MQ7_basic"
    "./examples/MQ8_basic"
    "./examples/MQ9_basic"
    "./examples/MQ131_basic"
    "./examples/MQ135_basic"
    "./examples/MQ307A_basic"
    "./examples/MQ309A_basic"
    "./examples/NMEAGPS_basic"
    "./examples/PMS5003_basic"
    "./examples/PPD42NS_basic"
    "./examples/ScanAir_network"
    "./examples/ScanAir_wifi"
    "./examples/sensor_network"
)

curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh
./bin/arduino-cli config init
./bin/arduino-cli core update-index
./bin/arduino-cli core install arduino:avr
./bin/arduino-cli lib install WiFi
./bin/arduino-cli lib install "Adafruit BME680 Library@2.0.0"
./bin/arduino-cli lib install TinyGPS

mkdir /root/Arduino/libraries/ScanAir
cp -R * /root/Arduino/libraries/ScanAir

for sketch in ${examples[@]}; do
  ./bin/arduino-cli compile --fqbn arduino:avr:uno $sketch
done
