/**
 * @file ScanAir_network.ino
 * @author Paul BEAUDUC (@acerlorion)
 * @brief Use case of sending request directly through ScanAir instance.
 * The network functionnality is only needed for ScanAir users.
 * @version 0.1
 * @date 2021-06-06
 * 
 * @copyright MIT License Copyright (c) 2021 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <ScanAirLib.h>

ScanAir instance;

const char ssid[] = "your_wifi_name_here";
const char wifipwd[] = "your_wifi_password_here";
const char secret[] = "your_secret_here";
const char name[] = "your_device_name_here"; // can be anything but must be the same name as the one on your ScanAir dashboard

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  int status = instance.connect(ssid, wifipwd, secret, name); // status can be one of those : ERROR_CONNECT_WIFI -1 | ERROR_CONNECT_SERVER 1 | SUCCESS_CONNECT 0
  if (status != SUCCESS_CONNECT) {
    Serial.println("Impossible to use the network!");
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  float value = 10.0f;

  instance.send("sensor_reference", "data_name", "data_unit", value); // create some raw data on our ScanAir server

  // or with ArduinoJson lib, this method is best for sending multiple data but is not recommended for beginners
  const size_t capacity = 256; // The size needed to create the JSON document
  StaticJsonDocument<capacity> json; // create your json document
  json["data"][0]["name"] = "data_name";
  json["data"][0]["value"] = value;
  json["data"][0]["unit"] = "data_unit";
  instance.send<capacity>("sensor_reference", json); // finally send the data to our ScanAir server

  delay(10000);
}
