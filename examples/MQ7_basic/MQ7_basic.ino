/**
 * @file MQ7_basic.ino
 * @author Paul BEAUDUC (@acerlorion)
 * @brief Basic use case of Scan'air library with MQ7 sensor.
 * @version 0.1
 * @date 2021-06-06
 * 
 * @copyright MIT License Copyright (c) 2021 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <ScanAirLib.h>
#include <MQ7/MQ7.h>

int pin_data = A0;

ScanAir instance; // creating your ScanAir instance
MQ7 sensor = instance.create<MQ7>(pin_data); // creating your MQ7 sensor

void setup() {
  // put your setup code here, to run once:
  sensor.setup();
}

void loop() {
  // put your main code here, to run repeatedly:
  sensor.compute(); // your sensor needs to compute data before you can get it

  float co_concentration = sensor.getCO(); // get the CO concentration in ppm

  delay(10000);
}
