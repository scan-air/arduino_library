#!/bin/bash

if [[ -d "./cov" ]]; then
    rm -rf cov
fi
mkdir cov
cd cov

cmake ..
make
./scanair_test

rm -rf CMakeFiles/scanair_test.dir/tests
lcov -c -d CMakeFiles/scanair_test.dir/ -o cov-info
lcov -r cov-info "/usr/include/*" "/usr/local/*" "*/arduino_library/tests/*" "*/arduino_library/src/include/*" -o filtered_cov.info
genhtml filtered_cov.info -o out
echo "To view coverage go to: arduino_library/cov/out/index.html"
