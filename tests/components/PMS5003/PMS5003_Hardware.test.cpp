/*
 * PMS5003.test.cpp
 * File description:
 * Mocked test for PMS5003 sensor
 */

#include "include/catch.hpp"
#include "PMS5003.h"
#include "ScanAirLib.h"
#include "MockHardwareSerial.hpp"

typedef MockHardwareSerial HardwareSerial;

TEST_CASE("PMS5003 sensor w/ HardwareSerial", "[PMS5003]") {
    HardwareSerial serial;
    ScanAir instance;
    PMS5003 sensor = instance.create<PMS5003>(serial);

    SECTION("should setup himself") {
        int status = sensor.setup();

        REQUIRE(status == 0);
    }

    SECTION("should not compute because of Serial not being available") {
        int status = sensor.setup();

        REQUIRE(status == 0);

        status = sensor.compute();
        REQUIRE(status == 1);
    }

    sensor.setState(1);
    SECTION("should not compute because not enough byte are available") {
        int status = sensor.setup();
     
        REQUIRE(status == 0);

        status = sensor.compute();
        REQUIRE(status == 1);

        status = sensor.compute();
        REQUIRE(status == 1);
    }

    sensor.setState(32);
    sensor.setCheckSum(1);
    SECTION("should not compute because checksum is wrong") {
        int status = sensor.setup();

        REQUIRE(status == 0);

        status = sensor.compute();
        REQUIRE(status == 1);

        status = sensor.compute();
        REQUIRE(status == 1);
    }

    sensor.setCheckSum(0);
    SECTION("should compute without problems") {
        int status = sensor.setup();

        REQUIRE(status == 0);

        status = sensor.compute();
        REQUIRE(status == 1);

        status = sensor.compute();
        REQUIRE(status == 0);
    }

    SECTION("should give the PM2.5 concentration") {
        int status = sensor.setup();

        REQUIRE(status == 0);

        status = sensor.compute();
        REQUIRE(status == 1);

        status = sensor.compute();
        REQUIRE(status == 0);

        float value = sensor.getPM25();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();

        REQUIRE(status == 0);

        status = sensor.compute();
        REQUIRE(status == 1);

        status = sensor.compute();
        REQUIRE(status == 0);

        status = sensor.sendDataToServer();
        REQUIRE(status == 0);
    }
}
