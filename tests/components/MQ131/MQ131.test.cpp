/*
 * MQ131.test.cpp
 * File description:
 * Mocked test for MQ131 sensor
 */

#include "include/catch.hpp"
#include "MQ131.h"
#include "ScanAirLib.h"

TEST_CASE("MQ131 sensor", "[MQ131]") {
    ScanAir instance;
    MQ131 low = instance.create<MQ131>(D1, D2, LOW_CONCENTRATION);
    MQ131 high = instance.create<MQ131>(D1, D2, HIGH_CONCENTRATION);
    MQ131 special = instance.create<MQ131>(D1, D2, SN_O2_LOW_CONCENTRATION);
    MQ131 nothappening = instance.create<MQ131>(D1, D2, 100);

    SECTION("should setup themselves") {
        int status = low.setup();
        REQUIRE(status == 0);

        status = high.setup();
        REQUIRE(status == 0);

        status = special.setup();
        REQUIRE(status == 0);
    }

    circuit[D2] = 1;
    SECTION("should compute without problems") {
        int status = low.setup();
        REQUIRE(status == 0);

        status = high.setup();
        REQUIRE(status == 0);

        status = special.setup();
        REQUIRE(status == 0);
        
        status = low.compute();
        REQUIRE(status == 0);

        status = high.compute();
        REQUIRE(status == 0);

        status = special.compute();
        REQUIRE(status == 0);
    }

    circuit[D2] = 0;
    SECTION("should compute with temperature and humidity without problems") {
        int status = low.setup();
        REQUIRE(status == 0);

        status = high.setup();
        REQUIRE(status == 0);

        status = special.setup();
        REQUIRE(status == 0);
        
        status = low.compute(20, 76);
        REQUIRE(status == 0);

        status = high.compute(20, 51);
        REQUIRE(status == 0);

        status = special.compute(20, 30);
        REQUIRE(status == 0);
    }

    SECTION("should give us the 03 concentration") {
        int status = low.setup();
        
        REQUIRE(status == 0);
        
        status = low.compute();

        REQUIRE(status == 0);

        float concentration = low.getO3();
    }

    SECTION("should send the computed data to the server") {
        int status = low.setup();
        
        REQUIRE(status == 0);
        
        status = low.compute();

        REQUIRE(status == 0);

        status = low.sendDataToServer();

        REQUIRE(status == 0);
    }
    circuit[D1] = LOW;
}
