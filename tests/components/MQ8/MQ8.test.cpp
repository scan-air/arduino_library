/*
 * MQ8.test.cpp
 * File description:
 * Mocked test for MQ8 sensor
 */

#include "include/catch.hpp"
#include "MQ8.h"
#include "ScanAirLib.h"

TEST_CASE("MQ8 sensor", "[MQ8]") {
    ScanAir instance;
    MQ8 sensor = instance.create<MQ8>(D1);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the H2 concentration") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float concentration = sensor.getH2();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
