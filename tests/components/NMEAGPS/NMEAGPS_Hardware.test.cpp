/*
 * NMEAGPS.test.cpp
 * File description:
 * Mocked test for NMEAGPS sensor
 */

#include "include/catch.hpp"
#include "NMEAGPS.h"
#include "ScanAirLib.h"
#include "MockHardwareSerial.hpp"

typedef MockHardwareSerial HardwareSerial;

TEST_CASE("NMEAGPS sensor w/ Hardware", "[NMEAGPS]") {
    HardwareSerial serial;
    ScanAir instance;
    NMEAGPS sensor = instance.create<NMEAGPS>(serial);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should not compute because of Serial not being available") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 1);
    }

    sensor.setState(1);
    SECTION("should not compute because location is not valid") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);

        status = sensor.compute();
        
        REQUIRE(status == 1);
    }
    
    sensor.getTinyGPS().encoding = true;
    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);

        status = sensor.compute();
        
        REQUIRE(status == 0);
    }

    SECTION("should give the latitude") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);

        status = sensor.compute();
        
        REQUIRE(status == 0);

        float latitude = sensor.getLatitude();

        REQUIRE(latitude == 0);
    }

    SECTION("should give the longitude") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);

        status = sensor.compute();
        
        REQUIRE(status == 0);

        float longitude = sensor.getLongitude();

        REQUIRE(longitude == 0);
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();

        REQUIRE(status == 0);

        status = sensor.compute();
        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}