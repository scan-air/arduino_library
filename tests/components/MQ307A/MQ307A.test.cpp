/*
 * MQ307A.test.cpp
 * File description:
 * Mocked test for MQ307A sensor
 */

#include "include/catch.hpp"
#include "MQ307A.h"
#include "ScanAirLib.h"

TEST_CASE("MQ307A sensor", "[MQ307A]") {
    ScanAir instance;
    MQ307A sensor = instance.create<MQ307A>(D1);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the CO concentration") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float concentration = sensor.getCO();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
