/*
 * MQ135.test.cpp
 * File description:
 * Mocked test for MQ135 sensor
 */

#include "include/catch.hpp"
#include "MQ135.h"
#include "ScanAirLib.h"

TEST_CASE("MQ135 sensor", "[MQ135]") {
    ScanAir instance;
    MQ135 sensor = instance.create<MQ135>(D1);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the concentration") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float concentration = sensor.getAirQuality();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
