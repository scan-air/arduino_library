/*
 * BME680.test.cpp
 * File description:
 * Mocked test for BME680 sensor
 */

#include "include/catch.hpp"
#include "BME680.h"
#include "ScanAirLib.h"

TEST_CASE("BME680 sensor", "[680]") {
    ScanAir instance;
    BME680 sensor = instance.create<BME680>();

    SECTION("should construct with parameters") {
        BME680 one = instance.create<BME680>(1);
        BME680 four = instance.create<BME680>(1, 2, 3, 4);
    }
    
    SECTION("should not setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 1);
    }

    sensor.setState(1);
    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should compute with problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
	    sensor.setState(0);
        status = sensor.compute();

        REQUIRE(status == 1);
    }

    sensor.setState(1);
    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the temperature") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float temperature = sensor.getTemperature();

        REQUIRE(temperature == 21.3f);
    }

    SECTION("should give us the pressure") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        uint32_t pressure = sensor.getPressure();

        REQUIRE(pressure == 100734);
    }

    SECTION("should give us the humidity") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float humidity = sensor.getHumidity();

        REQUIRE(humidity == 40);
    }

    SECTION("should give us the gas resistance") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        uint32_t gas_resistance = sensor.getGasResistance();

        REQUIRE(gas_resistance == 200);
    }

    SECTION("should give us the IAQ index") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        uint32_t iaq = sensor.getIAQ();

        REQUIRE(iaq == 375);
    }

    SECTION("should give us the altitude") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float altitude = sensor.getAltitude(/*Whatever*/10);

        REQUIRE(altitude == 2000);
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
