/*
 * MQ7.test.cpp
 * File description:
 * Mocked test for MQ7 sensor
 */

#include "include/catch.hpp"
#include "MQ7.h"
#include "ScanAirLib.h"

TEST_CASE("MQ7 sensor", "[MQ7]") {
    ScanAir instance;
    MQ7 sensor = instance.create<MQ7>(D1);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the CO concentration") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float concentration = sensor.getCO();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
