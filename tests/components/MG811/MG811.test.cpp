/*
 * MG811.test.cpp
 * File description:
 * Mocked test for MG811 sensor
 */

#include "include/catch.hpp"
#include "MG811.h"
#include "ScanAirLib.h"

TEST_CASE("MG811 sensor", "[MG811]") {
    ScanAir instance;
    MG811 sensor = instance.create<MG811>(A0, D1);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }
    
    SECTION("should compute with problems") {
        MG811 bad_sensor = instance.create<MG811>(A1, D2);
        int status = bad_sensor.setup();
        
        REQUIRE(status == 0);
        circuit[A1] = 1000;
        status = bad_sensor.compute();
        circuit[A1] = 0;
        REQUIRE(status == 1);
    }

    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the CO2 concentration") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float concentration = sensor.getCO2();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
