/*
 * MP503.test.cpp
 * File description:
 * Mocked test for MP503 sensor
 */

#include "include/catch.hpp"
#include "MP503.h"
#include "ScanAirLib.h"

TEST_CASE("MP503 sensor", "[MP503]") {
    ScanAir instance;
    MP503 sensor = instance.create<MP503>(D1);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the air quality") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float concentration = sensor.getAirQuality();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
