/*
 * PPD42NS.test.cpp
 * File description:
 * Mocked test for PPD42NS sensor
 */

#include "include/catch.hpp"
#include "PPD42NS.h"
#include "ScanAirLib.h"

TEST_CASE("PPD42NS sensor", "[PPD42NS]") {
    ScanAir instance;
    PPD42NS sensor = instance.create<PPD42NS>(D1, 10);

    SECTION("should setup himself") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
    }

    SECTION("should compute without problems") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);
    }

    SECTION("should give us the concentration") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float concentration = sensor.getDustConcentration();
    }

    SECTION("should give us the low pulse occupancy") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        unsigned long pulse = sensor.getLowPulseOccupancy();
    }

    SECTION("should give us the ratio") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        float ratio = sensor.getRatio();
    }

    SECTION("should send the computed data to the server") {
        int status = sensor.setup();
        
        REQUIRE(status == 0);
        
        status = sensor.compute();

        REQUIRE(status == 0);

        status = sensor.sendDataToServer();

        REQUIRE(status == 0);
    }
}
