/*
 * MockSerial.h
 * File description:
 * Here to mock the Serial
 * used to print things.
 */

#ifndef MOCKSERIAL_HPP_
#define MOCKSERIAL_HPP_

#include <string>
#include <iostream>

class MockSerial
{
    public:
        MockSerial();
        ~MockSerial();

        MockSerial(const MockSerial &) = delete;
        MockSerial(MockSerial &&) = delete;
        const MockSerial &operator=(const MockSerial &) = delete;
        const MockSerial &operator=(MockSerial &&) = delete;

        int begin(int);

        void print(int);
        void print(const std::string &);
        void println(int);
        void println(const std::string &);
};

static MockSerial Serial;

#endif /* MOCKSERIAL_HPP_ */
