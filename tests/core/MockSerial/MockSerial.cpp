/*
 * MockSerial.cpp
 * File description:
 * MockSerial class implementation
 */

#include "MockSerial.hpp"

MockSerial::MockSerial()
{}

MockSerial::~MockSerial()
{}

int MockSerial::begin(int baud)
{
    return 0;
}

void MockSerial::print(int nbr)
{
    std::cout << nbr;
}

void MockSerial::print(const std::string &str)
{
    std::cout << str;
}

void MockSerial::println(int nbr)
{
    std::cout << nbr << std::endl;
}

void MockSerial::println(const std::string &str)
{
    std::cout << str << std::endl;
}