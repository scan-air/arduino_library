/*
 * MockSoftwareSerial.h
 * File description:
 * Here to mock SoftwareSerial library.
 */

#ifndef MOCKSOFTWARESERIAL_HPP_
#define MOCKSOFTWARESERIAL_HPP_

#include <cstring>

typedef struct softpms5003data_s {
    uint16_t framelength = 0;
    // Concentration Units (standard)
    uint16_t pm10_std = 0;
    uint16_t pm25_std = 0;
    uint16_t pm100_std = 0;
    // Concentration Units (environmental)
    uint16_t pm10_env = 0;
    uint16_t pm25_env = 0;
    uint16_t pm100_en = 0;
    // Number of particles > Xum / 0.1L air
    uint16_t particles_03um = 0; 
    uint16_t particles_05um = 0;
    uint16_t particles_10um = 0;
    uint16_t particles_25um = 0;
    uint16_t particles_50um = 0;
    uint16_t particles_100um = 0;
    // utils
    uint16_t unused = 0;
    uint16_t checksum = 0;
} softpms5003data_t;

class MockSoftwareSerial
{
  private:
    int _state = 0;
    int _byte = 0;
    softpms5003data_t _data;
  public:
    MockSoftwareSerial(int rx, int tx) {}
    ~MockSoftwareSerial() {}
    // For mock purpose
    void listen() { return; }
    void begin(int baud) { return; }
    int available() const { return _state; }
    char read() { _byte = 0x42; return 1; };
    int peek() const { return _byte; }
    void readBytes(uint8_t *buffer, int size) { memcpy((void *)buffer, (void *)&_data, 30); }
    // For test purpose
    void setState(int state) { _state = state; }
    void setCheckSum(uint16_t checksum) { _data.checksum = checksum; }
};

#endif /* MOCKSOFTWARESERIAL_HPP_ */
