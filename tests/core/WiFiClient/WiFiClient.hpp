/*
 * WifiClient.h
 * File description:
 * Here to mock the WifiClient
 * used to connect to internet.
 */

#ifndef WIFICLIENT_HPP_
#define WIFICLIENT_HPP_

#include "MockString.hpp"
typedef MockString String;

#define WL_IDLE_STATUS 0
#define WL_CONNECTED 1
#define WL_ERROR -1

class MockWiFi
{
    private:
        int _state = WL_IDLE_STATUS;
    public:
        MockWiFi() {}
        ~MockWiFi() = default;

        int begin(String ssid, String wifi_pwd) { return _state; }
        void setState(int state) { _state = state; }
};

class WiFiClient
{
    private:
        bool _state = false;
        String _msg;
    public:
        WiFiClient() {}
        ~WiFiClient() = default;

        bool connect(String addr, int port) { return _state; }
        int println(String msg = "\n") {
	    _msg = _msg + msg;
	    return 0;
	}
        size_t write(const uint8_t *s, size_t n) { return n; }
        size_t write(uint8_t c) { return 1; }
        std::string getMessage() { return _msg.getString(); }
        void setState(bool state) { _state = state; }
};

extern MockWiFi WiFi;

#endif /* WIFICLIENT_HPP_ */
