/*
 * MockTinyGPS.h
 * File description:
 * Here to mock the NMEAGPS from NeoGPS library
 */

#ifndef MOCKTINYGPS_HPP_
#define MOCKTINYGPS_HPP_

#include "include/env.h"

class MockTinyGPS
{
public:
    bool encoding = false;

    static constexpr float GPS_INVALID_F_ANGLE = 1000.0; 

    MockTinyGPS() {}
    ~MockTinyGPS() {}

    bool encode(char c) { return encoding; }
    void f_get_position(float *latitude, float *longitude, unsigned long *age) { return; }
};

#endif /* MOCKTINYGPS_HPP_ */