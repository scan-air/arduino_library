/*
 * MockLibBME680.h
 * File description:
 * Here to mock the BME680 Adafruit's library
 */

#ifndef MOCKLIBBME680_HPP_
#define MOCKLIBBME680_HPP_

#define BME680_OS_8X 0
#define BME680_OS_2X 1
#define BME680_OS_4X 2
#define BME680_FILTER_SIZE_3 3

class MockLibBME680
{
    private:
        int _state = 0;
    public:
        MockLibBME680() {}
        MockLibBME680(int cs) {}
        MockLibBME680(int cs, int mosi, int miso, int sck) {}
        ~MockLibBME680() {}

        /* Basic functions */
        int begin(void) const { return _state; }
        int performReading(void) const { return _state; }

        /* Setup functions */
        void setTemperatureOversampling(int s) const { return; }
        void setHumidityOversampling(int s) const { return; }
        void setPressureOversampling(int s) const { return; }
        void setIIRFilterSize(int s) const { return; }
        void setGasHeater(int temp, int time) const { return; }

        /* Data retriever */
        unsigned int readAltitude(int sea_level) const { return 2000; }

        float temperature = 21.3;
        float pressure = 100734;
        float humidity = 40;
        float gas_resistance = 200;

        /* Test-oriented functions */
        void setState(int v) { _state = v; }
};

#endif