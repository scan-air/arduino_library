/*
 * ScanAir.test.cpp
 * File description:
 * Mocked test for ScanAir class
 */

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "ScanAirLib.h"

TEST_CASE("ScanAir connection to the api", "[ScanAir]") {

    SECTION("should return an error when trying to connect without internet") {
        ScanAir instance;

        int status = instance.connect("ssid", "wifipwd", "secret", "name");

        REQUIRE(status == ERROR_CONNECT_WIFI);
    }

    WiFi.setState(WL_CONNECTED);
    SECTION("should return an error if the credentials are not accepted by the api") {
        ScanAir instance;

        int status = instance.connect("ssid", "wifipwd", "secret", "name");

        REQUIRE(status == ERROR_CONNECT_SERVER);
    }

    SECTION("should properly return a success return value") {
        ScanAir instance;

        instance.getClient().setState(true);
        int status = instance.connect("ssid", "wifipwd", "secret", "name");

        REQUIRE(status == SUCCESS_CONNECT);
    }

    SECTION("should send the wanted data to the server") {
        ScanAir instance;

        instance.getClient().setState(true);
        int status = instance.connect("ssid", "wifipwd", "secret", "name");
	    std::string res = "POST /api/em/data HTTP/1.1Host: http://scanair.com:6969Accept: */*Content-Type: application/jsonContent-Length: 85\n";

        REQUIRE(status == SUCCESS_CONNECT);

        status = instance.send("dummy", "name", "unit", 1);

        REQUIRE(status == 0);
        REQUIRE(instance.getClient().getMessage() == res);
    }

    SECTION("should send the wanted data to the server 2") {
        ScanAir instance;

        instance.getClient().setState(true);
        int status = instance.connect("ssid", "wifipwd", "secret", "name");
	    std::string res = "POST /api/em/data HTTP/1.1Host: http://scanair.com:6969Accept: */*Content-Type: application/jsonContent-Length: 85\n";

        REQUIRE(status == SUCCESS_CONNECT);

        unsigned int value = 1;
        status = instance.send("dummy", "name", "unit", value);

        REQUIRE(status == 0);
        REQUIRE(instance.getClient().getMessage() == res);
    }

    SECTION("should send the wanted data to the server 2") {
        ScanAir instance;

        instance.getClient().setState(true);
        int status = instance.connect("ssid", "wifipwd", "secret", "name");
	    std::string res = "POST /api/em/data HTTP/1.1Host: http://scanair.com:6969Accept: */*Content-Type: application/jsonContent-Length: 85\n";

        REQUIRE(status == SUCCESS_CONNECT);

        unsigned long value = 1;
        status = instance.send("dummy", "name", "unit", value);

        REQUIRE(status == 0);
        REQUIRE(instance.getClient().getMessage() == res);
    }
}
