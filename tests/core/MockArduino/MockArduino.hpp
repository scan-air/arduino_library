/*
 * MockArduino.h
 * File description:
 * Here to mock an arduino circuit.
 */

#ifndef MOCKARDUINO_HPP_
#define MOCKARDUINO_HPP_

#include <iostream>
#include <unordered_map>
#include <thread>
#include <vector>
#include <chrono>
#include <cmath>

#define D0 0
#define D1 1
#define D2 2
#define D3 3
#define D4 4
#define D5 5
#define D6 6
#define D7 7
#define D8 8
#define D9 9
#define D10 10
#define D11 11
#define D12 12
#define D13 13
#define A0 14
#define A1 15
#define A2 16
#define A3 17
#define A4 18
#define A5 19

#define HIGH 100
#define LOW 0

#define INPUT 0
#define OUTPUT 1

class MockArduino
{
    public:
        MockArduino(void (*)(), void (*)());
        ~MockArduino();

        MockArduino(const MockArduino &) = delete;
        MockArduino(MockArduino &&) = delete;
        const MockArduino &operator=(const MockArduino &) = delete;
        const MockArduino &operator=(MockArduino &&) = delete;

        void start();
        void stop();
    private:
        bool _running = false;

        void (*_setup)() = nullptr;
        void (*_loop)() = nullptr;
};

extern std::unordered_map<int, int> record;
extern std::vector<float> circuit;
extern std::chrono::steady_clock::time_point program_start;

void digitalWrite(int, int);
int digitalRead(int);
void analogWrite(int, float);
float analogRead(int);
unsigned long pulseIn(int, int, unsigned long = 1000);
unsigned long millis();
double pow(float, float, float = 0);
double pow(double, double, float = 0);
double pow(float, double, float = 0);

void pinMode(int, int);

void delay(unsigned int);

long map(long x, long in_min, long in_max, long out_min, long out_max);

const char *F(const char str[]);

#endif /* MOCKARDUINO_HPP_ */
