/*
 * MockArduino.cpp
 * File description:
 * MockArduino class implementation
 */

#include "MockArduino.hpp"

std::unordered_map<int, int> record;
std::vector<float> circuit(20, 0);
std::chrono::steady_clock::time_point program_start = std::chrono::steady_clock::now();

MockArduino::MockArduino(void (*setup)(), void (*loop)()) : _setup(setup), _loop(loop)
{
}

MockArduino::~MockArduino()
{
    _setup = nullptr;
    _loop = nullptr;
    record.clear();
    circuit.clear();
}

void MockArduino::start()
{
    if (_setup == nullptr && _loop == nullptr) {
        std::cerr << "No setup nor loop function defined." << std::endl;
        return;    
    }
    _setup();
    _running = true;
    while (_running == true) {
        _loop();
    }
}

void MockArduino::stop() {
    _running = false;
}

void pinMode(int pin, int mode)
{
    record[pin] = mode;
}

void digitalWrite(int pin, int value)
{
    circuit[pin] = value;
}

int digitalRead(int pin)
{
    return circuit[pin];
}
        
void analogWrite(int pin, int value)
{
   circuit[pin] = value;
}

float analogRead(int pin)
{
   return circuit[pin];
}

unsigned long pulseIn(int pin, int value, unsigned long timeout)
{
    auto start = std::chrono::steady_clock::now();
    
    while (circuit[pin] != value);
    
    auto end = std::chrono::steady_clock::now();
    auto diff = end - start;
    
    return diff.count();
}

void delay(unsigned int ms)
{
    std::this_thread::sleep_for(std::chrono::microseconds(ms));
}

unsigned long millis()
{
    auto end = std::chrono::steady_clock::now();
    auto diff = end - program_start;

    return diff.count();
}

double pow(float base, float exponent, float unused)
{
    return std::pow(base, exponent);
}

double pow(double base, double exponent, float unused)
{
    return std::pow(base, exponent);
}

double pow(float base, double exponent, float unused)
{
    return std::pow(base, exponent);
}

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

const char *F(const char str[])
{
    return str;
}