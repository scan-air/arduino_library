/*
 * MockString.hpp
 * File description:
 * Arduino got his own String implementation
 * so do I.
 */

#ifndef MOCKSTRING_H_
#define MOCKSTRING_H_

#include <string>
#include <type_traits>

class MockString
{
    private:
        std::string _string;
    public:
        MockString() {}

        MockString(std::string string) : _string(string) {}
        MockString(const char string[]) : _string(string) {}

        template<typename T>
        MockString(T value) {
            if constexpr (std::is_arithmetic<T>::value == true) {
                _string = std::to_string(value);
            }
        }

        ~MockString() { _string.clear(); }

        const char *c_str() const { return _string.c_str(); }
        const std::string &getString() const { return _string; }
        MockString operator+(MockString &string) { return _string + string.getString(); }
        MockString operator+(const char string[]) { return std::string(string) + _string; } 
        bool operator==(const MockString &string) { return _string == string.getString(); }
};

bool operator==(const MockString &, const MockString &);
const std::string operator+(const char[], const MockString &);
const std::string operator+(const std::string &, const MockString &);

#endif /* MOCKSTRING_H_ */
