/*
 * MockString.cpp
 * File description:
 * MockString implementation
 */

#include "MockString.hpp"

const std::string operator+(const char lstr[], const MockString &rstr)
{
    return std::string(lstr) + rstr.getString();
}

const std::string operator+(const std::string &lstr, const MockString &rstr)
{
    return lstr + rstr.getString();
}

bool operator==(const MockString &lstr, const MockString &rstr)
{
    return lstr.getString() == rstr.getString();
}

