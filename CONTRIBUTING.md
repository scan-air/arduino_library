## You want to contribute some code?

We are always looking for quality contributions and will be happy to accept your Pull/Merge Requests as long as they adhere to some basic rules:

* Please make sure that your contribution fits well in the project's context:
  * we are aiming at building a really simple utility, with the least external dependencies possible;
* Please assure that you are submitting quality code, specifically make sure that:
  * your C++ code respect the latest conventions introduced by C++17.
  * your commits conform to the conventions established [here](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#)
