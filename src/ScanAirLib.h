/**
 * @file ScanAir.h
 * @author Paul Beauduc (@acerlorion)
 * @brief ScanAir class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SCANAIR_H_
#define SCANAIR_H_

#include "include/ArduinoJson.h"
#include "include/env.h"

#if defined(TEST)
    #include "WiFiClient.hpp"
#else
    #if defined(__AVR_ATmega4809__)
        #include <WiFiNINA.h>
    #else
        #include <WiFi.h>
    #endif
#endif

#include "Type.h"

/** @namespace sa
 * @brief ScanAir utility constant namespace.
 * 
 * In this namespace is defined the network constant.
 * 
 * For the return of WiFiClient error or success, theses are defined :
 * ERROR_CONNECT_WIFI -1 | ERROR_CONNECT_SERVER 1 | SUCCESS_CONNECT 0
 * 
 * The http address of ScanAir is also defined here. 
 */
namespace sa {
    #define ERROR_CONNECT_WIFI 2
    #define ERROR_CONNECT_SERVER 1
    #define SUCCESS_CONNECT 0

    #if defined (TEST)
        static const char server[] = "scanair.com";
    #else
        static const char server[] = "x2022scanair3705567454000.francecentral.cloudapp.azure.com";
    #endif
};

/** @class ScanAir
* @brief ScanAir class.
* 
* This class is the core of the ScanAir arduino library.
* It helps you create your sensors classes and abstract the network functionnalities.
*/
class ScanAir
{
    private:
        WiFiClient _client;
        uint16_t _port = 6969;
        String _secret;
        String _name;
    public:
        /**
         * @brief Construct a new Scan Air object
         * 
         * Use this to construct your ScanAir instance.
         */
        ScanAir();

        /**
         * @brief Destroy the Scan Air object
         */
        ~ScanAir();

        /**
         * @brief Construct a new Scan Air object from another constant one
         * 
         * Delete C++11 behaviour.
         * 
         * @param other another constant ScanAir object.
         */
        ScanAir(const ScanAir &other) = delete;

        /**
         * @brief Construct a new Scan Air object form another one
         * 
         * Delete C++11 behaviour.
         * 
         * @param other another ScanAir object.
         */
        ScanAir(ScanAir &&other) = delete;

        ScanAir &operator=(const ScanAir &) = delete;
        ScanAir &operator=(ScanAir &&) = delete;

        /**
        * @brief ScanAir connect function
        * 
        * Use WiFiClient to connect your handmade sensor to your WiFi network.
        * 
        * @param ssid the name of your WiFi network.
        * @param wifi_pwd your WiFi password.
        * @param secret your ScanAir secret that can be found on your ScanAir profile.
        * @param name the name of your handmade sensor.
        * 
        * @return ERROR_CONNECT_WIFI 2 | ERROR_CONNECT_SERVER 1 | SUCCESS_CONNECT 0 as byte
        */
        byte connect(const char ssid[], const char wifi_pwd[], const char secret[], const char name[]);

        /**
        * @brief ScanAir WiFiClient getter
        * 
        * Use this function to get the underlying WiFiClient
        * 
        * @return WiFiClient
        */
        WiFiClient &getClient();

        /**
        * @brief ScanAir templated send function
        * 
        * Use WiFiClient to send sensor's data to ScanAir's API.
        * 
        * @tparam T the data type to send. Ex: float
        * @param ref the sensor's reference. Ex: "BME680"
        * @param name the name of the data. Ex: "tempereture"
        * @param unit the unit of the data. Ex: "°C"
        * @param data the value of the data. Can be of those types: int, float, unsigned short, unsigned int. Ex: "20"
        * 
        * @return 1 if bad, 0 if good as byte. For now always 0.
        */
        template<typename T>
        byte send(const char ref[], const char name[], const char unit[], T data);

        /**
        * @brief ScanAir send function using ArduinoJson
        * 
        * Use WiFiClient to send sensor's data to ScanAir's API with ArduinoJson.
        * 
        * @tparam capacity of the json.
        * @param ref the sensor's reference. Ex: "BME680"
        * @param json a dynamic json containing the name, the unit and the value of the data. Ex:
        * {
        *   name: "temperature",
        *   unit: "°C",
        *   value: 20
        * }
        * 
        * @return 1 if bad, 0 if good as byte. For now always 0.
        */
        template<size_t capacity>
        byte send(const char ref[], StaticJsonDocument<capacity> &json)
        {
            json["secret"] = _secret.c_str();
            json["name"] = _name.c_str();
            json["reference"] = ref;
            _client.println("POST /api/em/data HTTP/1.1");
            _client.println("Host: " + String(sa::server) + ":6969");
            _client.println("Accept: */*");
            _client.println("Content-Type: application/json");
            _client.println("Content-Length: " + String(measureJsonPretty(json)));
            _client.println();
            serializeJsonPretty(json, _client);
            return 0;
        }

        /**
        * @brief ScanAir create sensor function
        * 
        * This is a templated function that should be able to create every ISensor inheriting classes and no other ones.
        * 
        * @tparam T the type you want to create.
        * @tparam Args all the variadic types of the parameters.
        * @param params all the variadic parameters needed to create the T object.
        * 
        * @return the object T requested in the template syntax filled with this ScanAir instance.
        */
        template<typename T, typename... Args>
        T create(Args&& ...params)
        {
            return T(*this, type::forward<Args>(params)...);
        }
};

#endif /* SCANAIR_H_ */
