/**
 * @file MG811.h
 * @author Paul Beauduc (@acerlorion)
 * @brief MG811 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MG811_H_
#define MG811_H_

#include "include/env.h"

#include "../ISensor.h"
#include "../ScanAirLib.h"

/** @def READ_SAMPLE_INTERVAL
 * @brief Time needed between each read of the sample
 */
#define READ_SAMPLE_INTERVAL 50

/** @def READ_SAMPLE_TIMES
 * @brief Number of read needed of the sample
 */
#define READ_SAMPLE_TIMES 5

/** @def LOG_400
 * @brief Constant used to compute the ppm value
 * 
 * This is the value of the resistance for CO2 concentration at 400 ppm.
 */
#define LOG_400 2.602

/** @def LOG_1000
 * @brief Constant used to compute the ppm value
 * 
 * This is the value of the resistance for CO2 concentration at 1000 ppm.
 */
#define LOG_1000 3

/**
 * @brief Constant in the mesure model used to compute the ppm value
 * 
 * This is the value of the voltage for CO2 concentration at 400 ppm.
 */
#define LOG_400_VOLTAGE 0.323

/**
 * @brief Constant in the mesure model used to compute the ppm value
 * 
 * This is the value of the voltage for CO2 concentration at 10000 ppm.
 */
#define LOG_1000_VOLTAGE 0.304


/** @class MG811
 * @brief MG811 ScanAir class
 * 
 * This class encapsulate the behaviour of the MG811 sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class MG811 : public ISensor
{
    private:
        float _co2_log_slope;;
        float _v400; 
        
        ScanAir &_instance;
        int _pin_out;
        int _pin_heater;
        int _value = 0;
    public:
        /**
         * @brief Construct a new MG811 object
         * 
         * @param instance ScanAir instance.
         * @param pin_out pin where the data is read.
         * @param pin_heater pin to control the sensor's heater.
         */
        MG811(ScanAir &instance, byte pin_out, byte pin_heater);

        /**
         * @brief Destroy the MG811 object
         */
        ~MG811() final;

        /**
         * @brief Construct a new MG811 object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another constant MG811 object.
         */
        MG811(const MG811 &other) = default;

        /**
         * @brief Construct a new MG811 object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MG811 object.
         */
        MG811(MG811 && other) = default;

        MG811 &operator=(const MG811 &) = default;
        MG811 &operator=(MG811 &&) = delete;

        /**
         * @brief MG811 sendDataToServer function
         * 
         * This function send to the ScanAir's API the C02 concentration
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief MG811 setup function
         * 
         * This function setup the MG811 sensor and its heater.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief MG811 compute function
         * 
         * This function compute the CO2 concentration from the MG811 voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief Get the CO2 concentration in ppm
         * 
         * @return the CO2 concentration as float.
         */
        float getCO2() const;
};

#endif /* MG811_H_ */
