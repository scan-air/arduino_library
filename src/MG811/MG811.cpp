/**
 * @file MG811.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief MG811 class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "MG811.h"

MG811::MG811(ScanAir &instance, byte pin_out, byte pin_heater)
    : _instance(instance), _pin_out(pin_out), _pin_heater(_pin_heater)
{
    _co2_log_slope = (LOG_1000_VOLTAGE - LOG_400_VOLTAGE) / (LOG_1000 - LOG_400);
    _v400 = LOG_400_VOLTAGE - _co2_log_slope * LOG_400;
}

MG811::~MG811()
{}

byte MG811::sendDataToServer()
{
    _instance.send("MG811", "co2_concentration", "ppm", _value);
    return 0;
}

byte MG811::setup()
{
    pinMode(_pin_heater, INPUT);
    digitalWrite(_pin_heater, HIGH);

    pinMode(_pin_out, INPUT);
    return 0;
}

byte MG811::compute()
{
    int cpt = 0;

    while (cpt < READ_SAMPLE_TIMES) {
        _value += analogRead(_pin_out);
        delay(READ_SAMPLE_INTERVAL);
        ++cpt;
    }
    _value = (_value / READ_SAMPLE_TIMES) * 5.0f / 1024.0f;
    if (_value >= LOG_400_VOLTAGE) {
        return 1;
    }
    _value = pow(10, (_value - _v400) / _co2_log_slope);
    return 0;
}

float MG811::getCO2() const
{
    return _value;
}
