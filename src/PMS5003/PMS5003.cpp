/**
 * @file PMS5003.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief PMS5003 class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "PMS5003.h"

PMS5003::PMS5003(ScanAir &instance, SoftwareSerial &serial)
    : _instance(instance), _soft(&serial)
{}

PMS5003::PMS5003(ScanAir &instance, HardwareSerial &serial)
    : _instance(instance), _hard(&serial)
{}

PMS5003::~PMS5003()
{}

byte PMS5003::sendDataToServer()
{
    _instance.send("PMS5003", "pm2.5_concentration", "μg/m3", _data.particles_25um);
    return 0;
}

byte PMS5003::setup()
{
    if (_soft != nullptr) {
        _soft->begin(9600);
    }
    if (_hard != nullptr) {
        _hard->begin(9600);
    }
    return 0;
}

byte PMS5003::compute()
{
    if (_soft != nullptr) {
        return getData(_soft);
    }
    if (_hard != nullptr) {
        return getData(_hard);
    }
    return 0;
}

byte PMS5003::getData(SoftwareSerial *serial)
{
    serial->listen();
    if (!serial->available()) {
        return 1;
    }
  
    // Read a byte at a time until we get to the special '0x42' start-byte
    if (serial->peek() != 0x42) {
        serial->read();
        return 1;
    }

    // Now read all 32 bytes
    if (serial->available() < 32) {
        return 1;
    }

    uint8_t buffer[32];    
    uint16_t sum = 0;
    serial->readBytes(buffer, 32);
 
    for (uint8_t i = 0; i < 30; i++) {
        sum += buffer[i];
    }

    // The data comes in endian'd, this solves it so it works on all platforms
    uint16_t buffer_u16[15];
    for (uint8_t i = 0; i < 15; i++) {
        buffer_u16[i] = buffer[2 + i * 2 + 1];
        buffer_u16[i] += (buffer[2 + i * 2] << 8);
    }

    memcpy((void *)&_data, (void *)buffer_u16, 30);
    if (sum != _data.checksum) {
        Serial.println("PMS5003(compute failed): checksum failure");
        return 1;
    }
    return 0;
}

byte PMS5003::getData(HardwareSerial *serial)
{
    if (!serial->available()) {
        return 1;
    }
  
    // Read a byte at a time until we get to the special '0x42' start-byte
    if (serial->peek() != 0x42) {
        serial->read();
        return 1;
    }

    // Now read all 32 bytes
    if (serial->available() < 32) {
        return 1;
    }

    uint8_t buffer[32];    
    uint16_t sum = 0;
    serial->readBytes(buffer, 32);
 
    for (uint8_t i = 0; i < 30; i++) {
        sum += buffer[i];
    }

    // The data comes in endian'd, this solves it so it works on all platforms
    uint16_t buffer_u16[15];
    for (uint8_t i = 0; i < 15; i++) {
        buffer_u16[i] = buffer[2 + i * 2 + 1];
        buffer_u16[i] += (buffer[2 + i * 2] << 8);
    }

    memcpy((void *)&_data, (void *)buffer_u16, 30);
    if (sum != _data.checksum) {
        Serial.println("PMS5003(compute failed): checksum failure");
        return 1;
    }
    return 0;
}

float PMS5003::getPM25() const
{
    return _data.particles_25um;
}