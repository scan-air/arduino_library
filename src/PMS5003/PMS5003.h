/**
 * @file PMS5003.h
 * @author Paul Beauduc (@acerlorion)
 * @brief PMS5003 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PMS5003_H_
#define PMS5003_H_

#include "include/env.h"

#if defined(TEST)
    #include "MockSoftwareSerial.hpp"
    #include "MockHardwareSerial.hpp"

    typedef MockSoftwareSerial SoftwareSerial;
    typedef MockHardwareSerial HardwareSerial;
#else
    #include <SoftwareSerial.h>
#endif

#include "../ISensor.h"
#include "../ScanAirLib.h"

/** @typedef struct pms5003data_s
 * @brief PMS5003 data structure used to communicate with the sensor
 * 
 * Contains the concentration units for standard and environmental case. Usually μg/m3.
 * 
 * Stock the PM concentration from PM0.3 to PM 10.0 with in-between PM0.5, PM1.0, PM2.5 and PM5.0.
 */
typedef struct pms5003data_s {
    uint16_t framelength;
    // Concentration Units (standard)
    uint16_t pm10_std;
    uint16_t pm25_std;
    uint16_t pm100_std;
    // Concentration Units (environmental)
    uint16_t pm10_env;
    uint16_t pm25_env;
    uint16_t pm100_env;
    // Number of particles > Xum / 0.1L air
    uint16_t particles_03um; 
    uint16_t particles_05um;
    uint16_t particles_10um;
    uint16_t particles_25um;
    uint16_t particles_50um;
    uint16_t particles_100um;
    // utils
    uint16_t unused;
    uint16_t checksum;
} pms5003data_t;


/** @class PMS5003
 * @brief PMS5003 ScanAir class
 * 
 * This class encapsulate the behaviour of the PMS5003 sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class PMS5003 : ISensor
{
    private:
        ScanAir &_instance;
        SoftwareSerial *_soft = nullptr;
        HardwareSerial *_hard = nullptr;
        pms5003data_t _data;

        byte getData(SoftwareSerial *serial);
        byte getData(HardwareSerial *serial);
    public:
        /**
         * @brief Construct a new PMS5003 object
         * 
         * @param instance ScanAir instance.
         * @param serial an already created SoftwareSerial object.
         */
        PMS5003(ScanAir &instance, SoftwareSerial &serial);

        /**
        * @brief Construct a new NMEAGPS object
        * 
        * @param instance ScanAir instance.
        * @param serial Hardware Serial of your Arduino board.
        */
        PMS5003(ScanAir &instance, HardwareSerial &serial);

        /**
         * @brief Destroy the PMS5003 object
         */
        ~PMS5003() final;

        /**
         * @brief Construct a new PMS5003 object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another PMS5003 constant object.
         */
        PMS5003(const PMS5003 &other) = default;

        /**
         * @brief Construct a new PMS5003 object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another PMS5003 object.
         */
        PMS5003(PMS5003 &&other) = default;

        PMS5003 &operator=(const PMS5003 &other) = default;
        PMS5003 &operator=(PMS5003 &&other) = delete;

        /**
         * @brief PMS5003 sendDataToServer function
         * 
         * This function send to the ScanAir's API the PM2.5 concentration.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief PMS5003 setup function
         * 
         * This function setup the sensor and a SoftwareSerial instance for communication purpose.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief MQ309A compute function
         * 
         * This function compute the PM2.5 concentration read from the PMS5003 data structure.
         * 
         * @see ISensor::compute()
         * @see struct pms5003data_s
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief Get the PM2.5 concentration in μg/m3
         * 
         * @return the PM2.5 concentration as float.
         */
        float getPM25() const;

        #if defined(TEST)
            void setState(int v)
            { 
                if (_soft != nullptr) _soft->setState(v);
                if (_hard != nullptr) _hard->setState(v);
            }
            void setCheckSum(uint16_t c)
            {
                if (_soft != nullptr) _soft->setCheckSum(c);
                if (_hard != nullptr) _hard->setCheckSum(c);
            }
        #endif
};

#endif /* PMS5003_H_ */
