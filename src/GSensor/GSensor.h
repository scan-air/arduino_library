/**
 * @file GSensor.h
 * @author Paul Beauduc (@acerlorion)
 * @brief GSensor class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef GSENSOR_H_
#define GSENSOR_H_

/** @def CALIBRATION_SAMPLE_TIMES
 * @brief Constant defined for the number of samples needed for calibration.
 */
#define CALIBRATION_SAMPLE_TIMES 50

/** @def CALIBRATION_SAMPLE_INTERVAL
 * @brief Constant defined for the time between samples needed for calibration.
 */
#define CALIBRATION_SAMPLE_INTERVAL 500

/** @def RL
 * @brief Constant used as the load Resistance.
 */
#define RL 10.0

#include "include/env.h"

/** @class GSensor
 * @brief GSensor class
 * 
 * Class used to abstract the workflow of the majority of gas sensors such as the MQ series.
 */
class GSensor
{
    public:
        /**
         * @brief Construct a new GSensor object
         * 
         * @param pin the arduino pin where you can get the data. Usually an analog.
         * @param r0 the value of the resistance in fresh air. Usually not known before calibration.
         * @param a the first coefficient used to determine the ppm value from the resistance value. 
         * @param b the second coefficient used to determine the ppm value from the resistance value.
         */
        GSensor(int pin, float r0, float a, float b);

        /**
         * @brief Destroy the GSensor object
         */
        ~GSensor();

        /**
         * @brief GSensor setup function
         * 
         * This function setup your gas sensor and calibrate it if needed.
         * 
         * @param clean_air_ratio the ratio in clean air usually written in official gas sensor's documentation.
         * @return 1 if bad, 0 if good as int. For now always 0.
         */
        int setup(float clean_air_ratio);

        /**
         * @brief GSensor update function
         * 
         * Update the ppm value of the gas sensor.
         * 
         * @return 1 if bad, 0 if good as int. For now always 0.
         */
        int update();

        /**
         * @brief GSensor getPPM getter
         *
         *  Get the ppm value of the gas sensor.
         * 
         * @return the ppm value as float.
         */
        float getPPM() const;

        /**
         * @brief GSensor R0 getter
         * 
         * Get the value of the resistance in fresh air given by the user or computed by the calibration.
         * 
         * @return the value of the resistance in fresh air as float.
         */
        float getR0() const;
    private:
        /**
         * @brief GSensor private calibrate function
         * 
         * Calibrate the gas sensor if the R0 given is 0.
         * 
         * @return 1 if bad, 0 if good as int. For now always 0. 
         */
        int calibrate(float);

        int _pin = 0;
        float _r0 = 0;
        float _a = 0;
        float _b = 0;
        float _value = 0;
        float _ratio = 0;
        float _ppm = 0;
};

#endif /* GSENSOR_H_ */