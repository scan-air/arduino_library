/**
 * @file GSensor.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief GSensor class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "GSensor.h"

GSensor::GSensor(int pin, float r0, float a, float b)
    : _pin(pin), _r0(r0), _a(a), _b(b)
{}

GSensor::~GSensor()
{}

int GSensor::setup(float clean_air_ratio)
{
    pinMode(_pin, INPUT);
    if (_r0 <= 0) {
        calibrate(clean_air_ratio);
    }
    return 0;
}

int GSensor::update()
{
    _value = analogRead(_pin) / 1023.0f * 5.0f;
    _value = ((5.0f * RL) / _value) - RL;
    _ratio = _value / _r0;
    _ppm = _a * pow(_ratio, _b); 
    return 0;
}

float GSensor::getPPM() const
{
    return _ppm;
}

float GSensor::getR0() const
{
    return _r0;
}

int GSensor::calibrate(float clean_air_ratio)
{
    int cpt = 0;
    float value = 0;
    float in = 0;

    while (cpt < CALIBRATION_SAMPLE_TIMES) {
        in = analogRead(_pin);
        value += ((float)RL * (1024 - in) / in);
        delay(CALIBRATION_SAMPLE_INTERVAL);
        ++cpt;
    }
    value = value / CALIBRATION_SAMPLE_TIMES;
    value = value / clean_air_ratio; 
    _r0 = value;
    return 0;
}