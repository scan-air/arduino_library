/**
 * @file ScanAir.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief ScanAir class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ScanAirLib.h"

ScanAir::ScanAir()
{}

ScanAir::~ScanAir()
{}

WiFiClient &ScanAir::getClient()
{
    return _client;
}

template<>
byte ScanAir::send<float>(const char ref[], const char name[], const char unit[], float data)
{
    StaticJsonDocument<128> json;
    JsonArray array = json.createNestedArray("data");
    
    json["secret"] = _secret.c_str();
    json["name"] = _name.c_str();
    json["reference"] = ref;
    array[0]["name"] = name;
    array[0]["value"] = data;
    array[0]["unit"] = unit;

    _client.println("POST /api/em/data HTTP/1.1");
    _client.println("Host: http://" + String(sa::server) + ":6969");
    _client.println("Accept: */*");
    _client.println("Content-Type: application/json");
    _client.println("Content-Length: " + String(measureJsonPretty(json)));
    _client.println();
    serializeJsonPretty(json, _client);
    return 0;
}

template<>
byte ScanAir::send<int>(const char ref[], const char name[], const char unit[], int data)
{
    StaticJsonDocument<128> json;
    JsonArray array = json.createNestedArray("data");
    
    json["secret"] = _secret.c_str();
    json["name"] = _name.c_str();
    json["reference"] = ref;
    array[0]["name"] = name;
    array[0]["value"] = data;
    array[0]["unit"] = unit;

    _client.println("POST /api/em/data HTTP/1.1");
    _client.println("Host: http://" + String(sa::server) + ":6969");
    _client.println("Accept: */*");
    _client.println("Content-Type: application/json");
    _client.println("Content-Length: " + String(measureJsonPretty(json)));
    _client.println();
    serializeJsonPretty(json, _client);
    return 0;
}

template<>
byte ScanAir::send<unsigned short>(const char ref[], const char name[], const char unit[], unsigned short data)
{
    StaticJsonDocument<128> json;
    JsonArray array = json.createNestedArray("data");
    
    json["secret"] = _secret.c_str();
    json["name"] = _name.c_str();
    json["reference"] = ref;
    array[0]["name"] = name;
    array[0]["value"] = data;
    array[0]["unit"] = unit;

    _client.println("POST /api/em/data HTTP/1.1");
    _client.println("Host: http://" + String(sa::server) + ":6969");
    _client.println("Accept: */*");
    _client.println("Content-Type: application/json");
    _client.println("Content-Length: " + String(measureJsonPretty(json)));
    _client.println();
    serializeJsonPretty(json, _client);
    return 0;
}

template<>
byte ScanAir::send<unsigned int>(const char ref[], const char name[], const char unit[], unsigned int data)
{
    StaticJsonDocument<128> json;
    JsonArray array = json.createNestedArray("data");
    
    json["secret"] = _secret.c_str();
    json["name"] = _name.c_str();
    json["reference"] = ref;
    array[0]["name"] = name;
    array[0]["value"] = data;
    array[0]["unit"] = unit;

    _client.println("POST /api/em/data HTTP/1.1");
    _client.println("Host: http://" + String(sa::server) + ":6969");
    _client.println("Accept: */*");
    _client.println("Content-Type: application/json");
    _client.println("Content-Length: " + String(measureJsonPretty(json)));
    _client.println();
    serializeJsonPretty(json, _client);
    return 0;
}

template<>
byte ScanAir::send<unsigned long>(const char ref[], const char name[], const char unit[], unsigned long data)
{
    StaticJsonDocument<128> json;
    JsonArray array = json.createNestedArray("data");
    
    json["secret"] = _secret.c_str();
    json["name"] = _name.c_str();
    json["reference"] = ref;
    array[0]["name"] = name;
    array[0]["value"] = data;
    array[0]["unit"] = unit;

    _client.println("POST /api/em/data HTTP/1.1");
    _client.println("Host: http://" + String(sa::server) + ":6969");
    _client.println("Accept: */*");
    _client.println("Content-Type: application/json");
    _client.println("Content-Length: " + String(measureJsonPretty(json)));
    _client.println();
    serializeJsonPretty(json, _client);
    return 0;
}

byte ScanAir::connect(const char ssid[], const char wifi_pwd[], const char secret[], const char name[])
{
    int status = WL_IDLE_STATUS;

    status = WiFi.begin(ssid, wifi_pwd);
    if (status != WL_CONNECTED) {
        return ERROR_CONNECT_WIFI;
    } else {
        if (_client.connect(sa::server, _port)) {
            _secret = secret;
            _name = name;
            return SUCCESS_CONNECT;
        }
        return ERROR_CONNECT_SERVER;
    }
}
