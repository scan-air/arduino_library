/**
 * @file MP503.h
 * @author Paul Beauduc (@acerlorion)
 * @brief MP503 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MP503_H_
#define MP503_H_

#include "include/env.h"

#include "../ISensor.h"
#include "../GSensor/GSensor.h"
#include "../ScanAirLib.h"

/** @def MP503_CLEAN_AIR_RATIO
 * @brief Constant defining the ratio, resistance and voltage, in fresh air for the MP503
 */
#define MP503_CLEAN_AIR_RATIO 1.0 // or maybe 3.6 same as the MQ135, it's not on the datasheet

/** @def MP503_COEFA
 * @brief First coefficient used to determine the ppm value from the resistance value.
 */
#define MP503_COEFA 1.0018

/** @def MP503_COEFB
 * @brief Second coefficient used to determine the ppm value from the resistance value.
 */
#define MP503_COEFB -1.4675

/** @class MP503
 * @brief MP503 ScanAir class
 * 
 * This class encapsulate the behaviour of the MP503 sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class MP503 : public ISensor
{
    private:
        ScanAir &_instance;
        GSensor _sensor;
        int _quality;
    public:
        /**
         * @brief Construct a new MP503 object
         * 
         * @param instance ScanAir instance.
         * @param pin Arduino pin where the data can be read. Usually A0.
         * @param r0 the value of the resistance in fresh air. Usually not known before calibration. Equal to 0 if not defined.
         */
        MP503(ScanAir &instance, byte pin, float r0 = 0);

        /**
         * @brief Destroy the MP503 object
         */
        ~MP503() final;

        /**
         * @brief Construct a new MP503 object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another constant MP503 object.
         */
        MP503(const MP503 &other) = default;

        /**
         * @brief Construct a new MP503 object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MP503 object.
         */
        MP503(MP503 &&other) = default;

        MP503 &operator=(const MP503 &other) = default;
        MP503 &operator=(MP503 &&other) = delete;

        /**
         * @brief MP503 sendDataToServer function
         * 
         * This function send to the ScanAir's API the air quality.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief MP503 setup function
         * 
         * This function setup the sensor and its pin.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief MP503 compute function
         * 
         * This function compute the air quality from the MP503 voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief Get the air quality in ppm
         * 
         * @return the air quality as float.
         */
        float getAirQuality() const;
};

#endif /* MP503_H_ */
