/**
 * @file MP503.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief MP503 class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "MP503.h"

MP503::MP503(ScanAir &instance, byte pin, float r0)
    : _instance(instance), _sensor(pin, r0, MP503_COEFA, MP503_COEFB)
{}

MP503::~MP503()
{}

byte MP503::sendDataToServer()
{
    _instance.send("MP503", "air_quality", "ppm", _quality);
    return 0;
}

byte MP503::setup()
{
    _sensor.setup(MP503_CLEAN_AIR_RATIO);
    return 0;
}

byte MP503::compute()
{
    _sensor.update();
    _quality = _sensor.getPPM();
    return 0;
}

float MP503::getAirQuality() const
{
    return _quality;
}
