/**
 * @file Type.h
 * @author Paul Beauduc (@acerlorion)
 * @brief #include <type_traits> && #include <utility> equivalent
 * 
 * Arduino doesn't have C++11 type_traits and utility headers
 * so here is what is needed for ScanAir to work
 * 
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TYPE_H_
#define TYPE_H_

/** @namespace type
* @brief ScanAir type namespace
* 
* As there is no std include while using Arduino.
* I had to remake std::forward() of the <utility> header.
* 
*  - std::forward() removes reference of an object. Now type::forward().
* 
* This function is used in the ScanAir create function. 
*/
namespace type
{
    // #include <utility>
    template<class T> struct remove_reference { typedef T type; };
    template<class T> struct remove_reference<T&> { typedef T type; };
    template<class T> struct remove_reference<T&&> { typedef T type; };

    template<class T>
    T&& forward (typename remove_reference<T>::type& t) noexcept
    {
        return static_cast<T&&>(t);
    };

    template<class T>
    T&& forward (typename remove_reference<T>::type&& t) noexcept
    {
        return static_cast<T&&>(t);
    };
};

#endif /* TYPE_H_ */
