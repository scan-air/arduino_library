/**
 * @file ISensor.h
 * @author Paul Beauduc (@acerlorion)
 * @brief ISensor interface definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ISENSOR_H_
#define ISENSOR_H_

#include "include/env.h"

/** @interface ISensor
* @brief ISensor interface.
* 
* As the name implies it, this interface is used for each implementation of a sensor.
* It shows the logical way of using a sensor implemented in the ScanAir arduino library.
*/
class ISensor
{
    public:
        virtual ~ISensor() = default;

        /**
        * @brief ISensor sendDataToServer virtual function
        * 
        * This function should send directly the data of a sensor to the ScanAir's API.
        * Has to be inherited to create a behaviour.
        * 
        * @return 1 if bad, 0 if good as byte.
        */
        virtual byte sendDataToServer() = 0;

        /**
         * @brief ISensor setup virtual function
         * 
         * This function should setup and, if needed, calibrate a sensor.
         * Has to be inherited to create a behaviour.
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        virtual byte setup() = 0;

        /**
         * @brief ISensor compute virtual function
         * 
         * This function should compute the data given by a sensor.
         * Has to be inherited to create a behaviour.
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        virtual byte compute() = 0;
};

#endif /* ISENSOR_H_ */
