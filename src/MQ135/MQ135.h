/**
 * @file MQ135.h
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ135 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQ135_H_
#define MQ135_H_

#include "include/env.h"

#include "../ISensor.h"
#include "../GSensor/GSensor.h"
#include "../ScanAirLib.h"

/** @def MQ135_CLEAN_AIR_RATIO
 * @brief Constant defining the ratio, resistance and voltage, in fresh air for the MQ135
 */
#define MQ135_CLEAN_AIR_RATIO 3.6

/** @def MQ135_RZERO
 * @brief Constant defining the value of the resistance in fresh air usually used.
 */
#define MQ135_RZERO 76.63

/** @def MQ135_COEFA
 * @brief First coefficient used to determine the ppm value from the resistance value.
 */
#define MQ135_COEFA 116.6020682

/** @def MQ135_COEFB
 * @brief Second coefficient used to determine the ppm value from the resistance value.
 */
#define MQ135_COEFB -2.769034857

/** @class MQ135
 * @brief MQ135 ScanAir class
 * 
 * This class encapsulate the behaviour of the MQ135 sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class MQ135 : public ISensor
{
    private:
        ScanAir &_instance;
        GSensor _sensor;
        float _quality;
    public:
        /**
         * @brief Construct a new MQ135 object
         * 
         * @param instance ScanAir instance.
         * @param pin Arduino pin where the data can be read. Usually A0.
         * @param r0 the value of the resistance in fresh air. Usually not known before calibration. Equal to MQ135_RZERO constant if not defined.
         */
        MQ135(ScanAir &instance, byte pin, float r0 = MQ135_RZERO);

        /**
         * @brief Destroy the MQ135 object
         */
        ~MQ135() final;

        /**
         * @brief Construct a new MQ135 object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ135 constant object.
         */
        MQ135(const MQ135 &other) = default;

        /**
         * @brief Construct a new MQ135 object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ135 object.
         */
        MQ135(MQ135 &&other) = default;

        MQ135 &operator=(const MQ135 &other) = default;
        MQ135 &operator=(MQ135 &&other) = delete;

        /**
         * @brief MQ135 sendDataToServer function
         * 
         * This function send to the ScanAir's API the air quality index.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief MQ135 setup function
         * 
         * This function setup the sensor and its pin.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief MQ135 compute function
         * 
         * This function compute the air quality index from the MQ8 voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief Get the air quality in ppm
         * 
         * @return the air quality as float
         */
        float getAirQuality() const;
};

#endif /* MQ135_H_ */
