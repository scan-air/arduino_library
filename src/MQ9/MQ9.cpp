/**
 * @file MQ9.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ9 class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "MQ9.h"

MQ9::MQ9(ScanAir &instance, byte pin, float r0)
    : _instance(instance), _sensor(pin, r0, MQ9_COEFA, MQ9_COEFB)
{}

MQ9::~MQ9()
{}

byte MQ9::sendDataToServer()
{
    _instance.send("MQ9", "co_concentration", "ppm", _value);
    return 0;
}

byte MQ9::setup()
{   
    _sensor.setup(MQ9_CLEAN_AIR_RATIO);
    return 0;
}

byte MQ9::compute()
{
    _sensor.update();
    _value = _sensor.getPPM();
    return 0;
}

float MQ9::getCO() const
{
    return _value;
}
