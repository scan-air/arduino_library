/**
 * @file MQ9.h
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ9 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQ9_H_
#define MQ9_H_

#include "include/env.h"

#include "../ISensor.h"
#include "../GSensor/GSensor.h"
#include "../ScanAirLib.h"

/** @def MQ9_CLEAN_AIR_RATIO
 * @brief Constant defining the ratio, resistance and voltage, in fresh air for the MQ9
 */
#define MQ9_CLEAN_AIR_RATIO 9.6

/** @def MQ9_COEFA
 * @brief First coefficient used to determine the ppm value from the resistance value.
 */
#define MQ9_COEFA 599.65

/** @def MQ9_COEFB
 * @brief Second coefficient used to determine the ppm value from the resistance value.
 */
#define MQ9_COEFB -2.244

/** @class MQ9
 * @brief MQ9 ScanAir class
 * 
 * This class encapsulate the behaviour of the MQ9 sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class MQ9 : public ISensor
{
    private:
        ScanAir &_instance;
        GSensor _sensor;
        float _value;
    public:
        /**
         * @brief Construct a new MQ9 object
         * 
         * @param instance ScanAir instance.
         * @param pin Arduino pin where the data can be read. Usually A0.
         * @param r0  the value of the resistance in fresh air. Usually not known before calibration. Equal to 0 if not defined.
         */
        MQ9(ScanAir &instance, byte pin, float r0 = 0);

        /**
         * @brief Destroy the MQ9 object
         */
        ~MQ9() final;

        /**
         * @brief Construct a new MQ9 object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ9 constant object.
         */
        MQ9(const MQ9 &other) = default;

        /**
         * @brief Construct a new MQ9 object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ9 object.
         */
        MQ9(MQ9 &&other) = default;

        MQ9 &operator=(const MQ9 &other) = default;
        MQ9 &operator=(MQ9 &&other) = delete;

        /**
         * @brief MQ9 sendDataToServer function
         * 
         * This function send to the ScanAir's API the C0 concentration.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief MQ9 setup function
         * 
         * This function setup the sensor and its pin.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief MQ9 compute function
         * 
         * This function compute the CO concentration from the MQ9 voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief Get the CO concentration in ppm 
         * 
         * @return the CO concentration as float.
         */
        float getCO() const;
};

#endif /* MQ9_H_ */
