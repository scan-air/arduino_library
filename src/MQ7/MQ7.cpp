/**
 * @file MQ7.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ7 class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "MQ7.h"

MQ7::MQ7(ScanAir &instance, byte pin, float r0)
    : _instance(instance), _sensor(pin, r0, MQ7_COEFA, MQ7_COEFB)
{}

MQ7::~MQ7()
{}

byte MQ7::sendDataToServer()
{
    _instance.send("MQ7", "co_concentration", "ppm", _value);
    return 0;
}

byte MQ7::setup()
{
    _sensor.setup(MQ7_CLEAN_AIR_RATIO);
    return 0;
}

byte MQ7::compute()
{
    _sensor.update();
    _value = _sensor.getPPM();
    return 0;
}

float MQ7::getCO() const
{
    return _value;
}
