/**
 * @file MQ7.h
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ7 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQ7_H_
#define MQ7_H_

#include "../include/env.h"

#include "../ISensor.h"
#include "../GSensor/GSensor.h"
#include "../ScanAirLib.h"

/** @def MQ7_CLEAN_AIR_RATIO
 * @brief Constant defining the ratio, resistance and voltage, in fresh air for the MQ7
 */
#define MQ7_CLEAN_AIR_RATIO 27.5

/** @def MQ7_COEFA
 * @brief First coefficient used to determine the ppm value from the resistance value.
 */
#define MQ7_COEFA 99.042

/** @def MQ7_COEFB
 * @brief Second coefficient used to determine the ppm value from the resistance value.
 */
#define MQ7_COEFB -1.518

/** @class MQ7
 * @brief MQ7 ScanAir class
 * 
 * This class encapsulate the behaviour of the MQ7 sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class MQ7 : public ISensor
{
    private:
        ScanAir &_instance;
        GSensor _sensor;
        float _value;
    public:
        /**
         * @brief Construct a new MQ7 object
         * 
         * @param instance ScanAir instance.
         * @param pin Arduino pin where the data can be read. Usually A0.
         * @param r0 the value of the resistance in fresh air. Usually not known before calibration. Equal to 0 if not defined.
         */
        MQ7(ScanAir &instance, byte pin, float r0 = 0);

        /**
         * @brief Destroy the MQ7 object
         */
        ~MQ7() final;

        /**
         * @brief Construct a new MQ7 object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ7 constant object.
         */
        MQ7(const MQ7 &other) = default;

        /**
         * @brief Construct a new MQ7 object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ7 object.
         */
        MQ7(MQ7 &&other) = default;

        MQ7 &operator=(const MQ7 &other) = default;
        MQ7 &operator=(MQ7 &&other) = delete;

        /**
         * @brief MQ7 sendDataToServer function
         * 
         * This function send to the ScanAir's API the CO concentration.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief MQ7 setup function
         * 
         * This function setup the sensor and its pin.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief MQ7 compute function
         * 
         * This function compute the CO concentration from the MQ7 voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief Get the CO concentration in ppm 
         * 
         * @return the CO concentration as float.
         */
        float getCO() const;
};

#endif /* MQ7_H_ */
