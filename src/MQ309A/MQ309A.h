/**
 * @file MQ309A.h
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ309A class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQ309A_H_
#define MQ309A_H_

#include "include/env.h"

#include "../ISensor.h"
#include "../GSensor/GSensor.h"
#include "../ScanAirLib.h"

/** @def MQ309A_CLEAN_AIR_RATIO
 * @brief Constant defining the ratio, resistance and voltage, in fresh air for the MQ309A
 */
#define MQ309A_CLEAN_AIR_RATIO 11

/** @def MQ309A_COEFA
 * @brief First coefficient used to determine the ppm value from the resistance value.
 */
#define MQ309A_COEFA 1000000

/** @def MQ309A_COEFB
 * @brief Second coefficient used to determine the ppm value from the resistance value.
 */
#define MQ309A_COEFB -4.01

/** @class MQ309A
 * @brief MQ309A ScanAir class
 * 
 * This class encapsulate the behaviour of the MQ309A sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class MQ309A : public ISensor
{
    private:
        ScanAir &_instance;
        GSensor _sensor;
        float _value;
    public:
        /**
         * @brief Construct a new MQ309A object
         * 
         * @param instance ScanAir instance.
         * @param pin Arduino pin where the data can be read. Usually A0.
         * @param r0 the value of the resistance in fresh air. Usually not known before calibration. Equal to 0 if not defined.
         */
        MQ309A(ScanAir &instance, byte pin, float r0 = 0);

        /**
         * @brief Destroy the MQ309A object
         */
        ~MQ309A() final;

        /**
         * @brief Construct a new MQ309A object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ309A constant object.
         */
        MQ309A(const MQ309A &other) = default;

        /**
         * @brief Construct a new MQ309A object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ309A object.
         */
        MQ309A(MQ309A &&other) = default;

        MQ309A &operator=(const MQ309A &other) = default;
        MQ309A &operator=(MQ309A &&other) = delete;

        /**
         * @brief MQ309A sendDataToServer function
         * 
         * This function send to the ScanAir's API the CO concentration.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;
        
        /**
         * @brief MQ309A setup function
         * 
         * This function setup the sensor and its pin.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;
        
        /**
         * @brief MQ309A compute function
         * 
         * This function compute the CO concentration from the MQ309A voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;


        /**
         * @brief Get the CO concentration in ppm 
         * 
         * @return the CO concentration as float.
         */
        float getCO() const;
};

#endif /* MQ309A_H_ */