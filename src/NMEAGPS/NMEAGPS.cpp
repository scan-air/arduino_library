/**
 * @file NMEAGPS.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief NMEAGPS class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "NMEAGPS.h"

NMEAGPS::NMEAGPS(ScanAir &instance, SoftwareSerial &serial)
    : _instance(instance), _soft(&serial)
{}

NMEAGPS::NMEAGPS(ScanAir &instance, HardwareSerial &serial)
    : _instance(instance), _hard(&serial)
{}

NMEAGPS::~NMEAGPS()
{}

byte NMEAGPS::setup()
{
    if (_soft != nullptr) {
        _soft->begin(9600);
    }
    if (_hard != nullptr) {
        _hard->begin(9600);
    }
    return 0;
}

byte NMEAGPS::sendDataToServer()
{
    StaticJsonDocument<256> json;
    JsonArray data = json.createNestedArray("data");

    data[0]["name"] = "latitude";
    data[0]["value"] = _latitude;
    data[0]["unit"] = "°";
    data[1]["name"] = "longitude";
    data[1]["value"] = _longitude;
    data[1]["unit"] = "°";
    _instance.send<256>("NMEAGPS", json);
    return 0;
}

byte NMEAGPS::compute()
{
    if (_soft != nullptr) {
        _soft->listen();
    #if defined(TEST)
        if (_soft->available()) {
    #else
        while (_soft->available()) {
    #endif
            char c = _soft->read();
            if (_gps.encode(c)) {
                _gps.f_get_position(&_latitude, &_longitude, &_age);
                _latitude = (_latitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : _latitude);
                _longitude = (_longitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : _longitude);
                return 0;
            }
        }
    }
    if (_hard != nullptr) {
    #if defined(TEST)
        if (_hard->available()) {
    #else
        while (_hard->available()) {
    #endif
            char c = _hard->read();
            if (_gps.encode(c)) {
                _gps.f_get_position(&_latitude, &_longitude, &_age);
                _latitude = (_latitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : _latitude);
                _longitude = (_longitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : _longitude);
                return 0;
            }
        }
    }
    return 1;
}

float NMEAGPS::getLatitude() const
{
    return _latitude;
}

float NMEAGPS::getLongitude() const
{
    return _longitude;
}