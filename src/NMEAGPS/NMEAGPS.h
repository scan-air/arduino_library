/**
 * @file NMEAGPS.h
 * @author Paul Beauduc (@acerlorion)
 * @brief NMEAGPS class definition
 * @version 0.1
 * @date 2021-06-26
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef NMEAGPS_H_
#define NMEAGPS_H_

#include "include/ArduinoJson.h"
#include "include/env.h"

#if defined(TEST)
    #include "../../tests/core/MockTinyGPS/MockTinyGPS.hpp"
    #include "MockSoftwareSerial.hpp"
    #include "MockHardwareSerial.hpp"

    typedef MockSoftwareSerial SoftwareSerial;
    typedef MockHardwareSerial HardwareSerial;
    typedef MockTinyGPS TinyGPS;
#else
    #include <TinyGPS.h>
    #include <SoftwareSerial.h>
#endif

#include "../ISensor.h"
#include "../ScanAirLib.h"

/** @class NMEAGPS
 * @brief NMEAGPS ScanAir class
 * 
 * This class encapsulate the behaviour of the NEO gps serie while following the life cycle determined in ISensor.h.
 * Can be used with every gps using NMEA protocol.
 * Use NMEAGPS library.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class NMEAGPS : public ISensor
{
private:
    ScanAir &_instance;
    SoftwareSerial *_soft = nullptr;
    HardwareSerial *_hard = nullptr;
    TinyGPS _gps;

    float _latitude = 0;
    float _longitude = 0;
    unsigned long _age = 0;
public:
    /**
     * @brief Construct a new NMEAGPS object
     * 
     * @param instance ScanAir instance.
     * @param serial an already created SoftwareSerial object. 
     */
    NMEAGPS(ScanAir &instance, SoftwareSerial &serial);

    /**
     * @brief Construct a new NMEAGPS object
     * 
     * @param instance ScanAir instance.
     * @param serial Hardware Serial of your Arduino board.
     */
    NMEAGPS(ScanAir &instance, HardwareSerial &serial);

    /**
     * @brief Destroy the NMEAGPS object
     */
    ~NMEAGPS() final;

    /**
     * @brief Construct a new NMEAGPS object from another constant one
     * 
     * Default C++11 behaviour.
     * 
     * @param other another PMS5003 constant object.
     */
    NMEAGPS(const NMEAGPS &other) = default;

    /**
     * @brief Construct a new NMEAGPS object from another one
     * 
     * Default C++11 behaviour.
     * 
     * @param other another PMS5003 object.
     */
    NMEAGPS(NMEAGPS &&other) = default;

    NMEAGPS &operator=(const NMEAGPS &) = default;
    NMEAGPS &operator=(NMEAGPS &&) = delete;

    /**
     * @brief NMEAGPS setup function
     * 
     * This function setup the sensor and the NMEAGPS library.
     * 
     * @see ISensor::setup()
     * 
     * @return 1 if bad, 0 if good as byte.
     */
    byte setup() final;

    /**
     * @brief NMEAGPS sendDataToServer function
     * 
     * This function send directly all the data to the ScanAir's API.
     * 
     * @see ISensor::sendDataToServer()
     * 
     * @return 1 if bad, 0 if good as byte.
     */
    byte sendDataToServer() final;

    /**
     * @brief NMEAGPS compute function
     * 
     * This function compute all the data given by the Adafruit_680 library.
     * This function also compute the IAQ index with the gas_resistance and humidity property.
     * gas_resistance is valued as 75% of weight and humidity is valued as 25% of weight.
     * gas_resistance ranges are : 50 - 50000 -> 75% - 0%
     * humidity ranges are : 40 - 100 -> 0% - 25% and 40 - 0 -> 0% - 25%
     * The final IAQ index is then mapped on a range going grom 0 to 500.
     * 
     * @see ISensor::compute()
     * 
     * @return 1 if bad, 0 if good as byte.
     */
    byte compute() final;

    /**
     * @brief Get the Latitude in degree
     * 
     * This value is positive for North degrees and negative for South degrees.
     * 
     * @return the Latitude as float.
     */
    float getLatitude() const;

    /**
     * @brief Get the Longitude in degree
     * 
     * This value is positive for East degrees and negative for West degrees.
     * 
     * @return the Longitude as float.
     */
    float getLongitude() const;

    /**
     * @brief Get the TinyGPS object from the TinyGPS Library
     * 
     * @return the reference of underlying TinyGPS object.
     */
    TinyGPS &getTinyGPS() { return _gps; }

    #if defined(TEST)
        void setState(int v)
        { 
            if (_soft != nullptr) _soft->setState(v);
            if (_hard != nullptr) _hard->setState(v);
        }
    #endif
};

#endif /* NMEAGPS_H_ */