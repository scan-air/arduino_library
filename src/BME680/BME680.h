/**
 * @file BME680.h
 * @author Paul Beauduc (@acerlorion)
 * @brief BME680 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef BME680_HPP_
#define BME680_HPP_

#if defined(TEST)
    #include "../../tests/core/MockLibBME80/MockLibBME680.hpp"

    typedef MockLibBME680 Adafruit_BME680;
#else
    #include <Adafruit_BME680.h>
    #include <bme68x.h>
    #include <bme68x_defs.h>
#endif

#include "include/ArduinoJson.h"
#include "include/env.h"

#include "../ISensor.h"
#include "../ScanAirLib.h"

/** @class BME680
 * @brief BME680 ScanAir class
 * 
 * Encapsulation of the Adafruit_BME680 library to be used with ScanAir.
 * Inherit from ISensor.
 * 
 * @see ISensor.h
 */
class BME680 : public ISensor
{
    public:
        /**
         * @brief Construct a new BME680 object
         * 
         * @param instance ScanAir instance.
         */
        BME680(ScanAir &instance);

        /**
         * @brief Construct a new BME680 object
         * 
         * @param instance ScanAir instance.
         * @param cs SPI chip select.
         */
        BME680(ScanAir &instance, byte cs);

        /**
         * @brief Construct a new BME680 object
         * 
         * @param instance ScanAir instace.
         * @param cs SPI chip select.
         * @param mosi SPI MOSI (Data from microcontroller to sensor).
         * @param miso SPI MISO (Data to microcontroller from sensor).
         * @param sck SPI Clock.
         */
        BME680(ScanAir &instance, byte cs, byte mosi, byte miso, byte sck);

        /**
         * @brief Destroy the BME680 object
         */
        ~BME680() final;

        /**
         * @brief Construct a new BME680 object from another constant one
         * 
         * Copy construct.
         * Default C++11 behaviour.
         * 
         * @param other another constant BME680 object.
         */
        BME680(const BME680 &other) = default;

        /**
         * @brief Construct a new BME680 object from another one
         * 
         * Move construct.
         * Default C++11 behaviour.
         * 
         * @param other another BME680 object.
         */
        BME680(BME680 &&other) = default;

        /**
         * @brief Assign a constant BME680 object to another one
         * 
         * Copy assign.
         * Default C++11 behaviour.
         * 
         * @param other another constant BME680 object.
         */
        BME680 &operator=(const BME680 &) = default;

        /**
         * @brief Assign a BME680 object to another one
         * 
         * Move assign.
         * Delete C++11 behaviour.
         * 
         * @param other another BME680 object.
         */
        BME680 &operator=(BME680 &&) = delete;

        /**
         * @brief BME680 setup function
         * 
         * This function setup the sensor and the Adafruit_BME680 library.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief BME680 sendDataToServer function
         * 
         * This function send directly all the coherent data to the ScanAir's API.
         * gas_resistance is the only data not sent.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief BME680 compute function
         * 
         * This function compute all the data given by the Adafruit_680 library.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;
    
        /**
         * @brief Get the temperature value
         * 
         * @return the temperature as float.
         */
        float getTemperature() const;

        /**
         * @brief Get the pressure value
         * 
         * @return the pressure as uint32_t.
         */
        uint32_t getPressure() const;

        /**
         * @brief Get the humidity value
         * 
         * @return the humidity as float.
         */
        float getHumidity() const;

        /**
         * @brief Get the Gas Resistance value
         * 
         * @return the resistance value (ohms) as uint32_t 
         */
        uint32_t getGasResistance() const;

        /**
         * @brief Get the gas resistance IAQ index
         * 
         * @return the IAQ index as ubyte.
         */
        uint16_t getIAQ() const;

        /**
         * @brief Get the altitude value
         * 
         * @param seaLevel the level of the sea.
         * If no args are passed 1013.25 is used.
         * 
         * @return the altitude as float.
         */
        float getAltitude(const float seaLevel = 1013.25f);

        #if defined(TEST)
            void setState(int v) { _sensor.setState(v); }
        #endif
  private:
        float _last_sea_level = 1013.25f;
        uint16_t _iaq = 0;
        ScanAir &_instance;
        Adafruit_BME680 _sensor;
};

#endif /* BME680_HPP_ */
