/**
 * @file BME680.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief BME680 class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "BME680.h"

BME680::BME680(ScanAir &instance) : _instance(instance)
{}

BME680::BME680(ScanAir &instance, byte cs)
    : _instance(instance), _sensor(cs)
{}

BME680::BME680(ScanAir &instance, byte cs, byte mosi, byte miso, byte sck)
    : _instance(instance), _sensor(cs, mosi, miso, sck)
{}

BME680::~BME680()
{}

byte BME680::setup()
{
    if (!_sensor.begin()) {
        return 1;
    }
    // Set up oversampling and filter initialization
    _sensor.setTemperatureOversampling(BME680_OS_8X);
    _sensor.setHumidityOversampling(BME680_OS_2X);
    _sensor.setPressureOversampling(BME680_OS_4X);
    _sensor.setIIRFilterSize(BME680_FILTER_SIZE_3);
    _sensor.setGasHeater(320, 150); // 320*C for 150 ms
    return 0;
}

byte BME680::sendDataToServer()
{
    StaticJsonDocument<368> json;
    JsonArray data = json.createNestedArray("data");

    data[0]["name"] = "temperature";
    data[0]["value"] = _sensor.temperature;
    data[0]["unit"] = "°C";
    data[1]["name"] = "pressure";
    data[1]["value"] = _sensor.pressure;
    data[1]["unit"] = "hPa";
    data[2]["name"] = "humidity";
    data[2]["value"] = _sensor.humidity;
    data[2]["unit"] = "%";
    data[3]["name"] = "iaq_index";
    data[3]["value"] = _iaq;
    data[3]["unit"] = "IAQ index";
    data[4]["name"] = "altitude";
    data[4]["value"] = _sensor.readAltitude(_last_sea_level);
    data[4]["unit"] = "m";
    _instance.send<368>("BME680", json);
    return 0;
}

byte BME680::compute()
{
    float hpart = 0;
    float gpart = 0;
    int8_t total = 0;
    
    if (!_sensor.performReading()) {
        Serial.println(F("BME680: Couldn't perform reading."));
        return 1;
    }
    
    if (_sensor.humidity > 40) {
        hpart = map(_sensor.humidity, 40, 100, 0, 25);
    } else {
        hpart = map(_sensor.humidity, 40, 0, 0, 25);
    }
    gpart = map(_sensor.gas_resistance, 50, 50000, 75, 0);
    
    total = hpart + gpart;
    if (total < 0) {
        total = 0;
    }
    
    _iaq = map(total, 0, 100, 0, 500);
    return 0;
}

float BME680::getTemperature() const
{
    return _sensor.temperature;
}

uint32_t BME680::getPressure() const
{
    return _sensor.pressure;
}

float BME680::getHumidity() const
{
    return _sensor.humidity;
}

uint32_t BME680::getGasResistance() const
{
    return _sensor.gas_resistance;
}

uint16_t BME680::getIAQ() const
{
    return _iaq;
}

float BME680::getAltitude(const float seaLevel)
{
    _last_sea_level = seaLevel;
    return _sensor.readAltitude(seaLevel);
}
