/**
 * @file MQ131.h
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ131 class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQ131_H_
#define MQ131_H_

#include "include/env.h"

#include "../ISensor.h"
#include "../ScanAirLib.h"

/** @def MQ131_DEFAULT_RL
 * @brief The default value of the resistance in series with the MQ131 sensor
 */
#define MQ131_DEFAULT_RL 1000000

/**
 * @brief Constant to determine the low concentration mode
 */
#define LOW_CONCENTRATION 0

/**
 * @brief Constant to determine the high concentration mode
 */
#define HIGH_CONCENTRATION 1

/**
 * @brief Constant to determine the special SN O2 low concentration mode
 */
#define SN_O2_LOW_CONCENTRATION 2

#if defined(TEST)
    #define MQ131_DEFAULT_STABLE_CYCLE -1

    #define MQ131_DEFAULT_LO_CONCENTRATION_TIMETOREAD 0
    #define MQ131_DEFAULT_HI_CONCENTRATION_TIMETOREAD 0
#else
    #define MQ131_DEFAULT_STABLE_CYCLE 15

    #define MQ131_DEFAULT_LO_CONCENTRATION_TIMETOREAD 80
    #define MQ131_DEFAULT_HI_CONCENTRATION_TIMETOREAD 80
#endif

/**
 * @brief Value of the resistance of the sensor in fresh air in low concentration mode
 */
#define MQ131_DEFAULT_LO_CONCENTRATION_R0 1917.22

/**
 * @brief Value of the resistance of the sensor in fresh air in high concentration mode
 */
#define MQ131_DEFAULT_HI_CONCENTRATION_R0 235.00

/** @class MQ131
 * @brief MQ131 ScanAir class
 * 
 * This class encapsulate the behaviour of the MQ131 sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class MQ131 : public ISensor
{
    private:
        ScanAir &_instance;
        byte _pinHeater;
        byte _pinValue;
        unsigned short _model;
        
        int _r0;
        int _timeToRead;
        bool _start = false;
        unsigned long _startMillis = 0;
        
        int _valueRL = MQ131_DEFAULT_RL;
        int _temperature;
        int _humidity;
        
        float _value = 0;

        float getEnvRatio();
        void calibrate();
        void computeRs();
        void computeIntoPPM();
    public:
        /**
         * @brief Construct a new MQ131 object
         * 
         * @param instance ScanAir instance.
         * @param pinHeater pin of the sensor's heater.
         * @param pinValue pin where you get the value.
         * @param type type of concentration. LOW_CONCENTRATION | HIGH_CONCENTRATION | SN_O2_LOW_CONCENTRATION
         * @param rl the value of the resistance in series with the MQ131 sensor.
         * @param temperature environment's temperature.
         * @param humidity environment's humidity.
         */
        MQ131(ScanAir &instance, byte pinHeater, byte pinValue, unsigned short type, int rl = MQ131_DEFAULT_RL, int temperature = 20, int humidity = 60);

        /**
         * @brief Destroy the MQ131 object
         */
        ~MQ131() final;

        /**
         * @brief Construct a new MQ131 object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ131 constant object
         */
        MQ131(const MQ131 &other) = default;

        /**
         * @brief Construct a new MQ131 object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another MQ131 object
         */
        MQ131(MQ131 &&other) = default;

        MQ131 &operator=(const MQ131 &other) = default;
        MQ131 &operator=(MQ131 &&other) = delete;

        /**
         * @brief MQ131 sendDataToServer function
         * 
         * This function send to the ScanAir's API the O3 concentration.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief MQ131 setup function
         * 
         * This function setup the sensor, its pin, its environment and the heater.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief MQ131 compute function
         * 
         * This function compute the O3 concentration from the MQ131 voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief MQ131 compute function with environment params
         *
         *  This funtion call the compute function inherited from ISensor after setting the environments constants.
         * 
         * @param temperature the environment's temperature.
         * @param humidity the environment's humidity.
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute(int temperature, int humidity);

        /**
         * @brief Get the O3 concentration in ppm
         * 
         * @return the O3 concentration as float
         */
        float getO3() const;
};

#endif /* MQ131_H_ */