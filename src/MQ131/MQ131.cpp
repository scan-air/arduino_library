/**
 * @file MQ131.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief MQ131 class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "MQ131.h"

MQ131::MQ131(ScanAir &instance, byte pinHeater, byte pinValue, unsigned short type, int rl, int temperature, int humidity)
    : _instance(instance), _pinHeater(pinHeater), _pinValue(pinValue), _model(type), _valueRL(rl), _temperature(temperature), _humidity(humidity)
{
    switch(_model) {
        case LOW_CONCENTRATION:
            _r0 = MQ131_DEFAULT_LO_CONCENTRATION_R0;
            _timeToRead = MQ131_DEFAULT_LO_CONCENTRATION_TIMETOREAD;
            break;
        case HIGH_CONCENTRATION:
            _r0 = MQ131_DEFAULT_HI_CONCENTRATION_R0;
            _timeToRead = MQ131_DEFAULT_HI_CONCENTRATION_TIMETOREAD;
            break;
        case SN_O2_LOW_CONCENTRATION:
            _r0 = MQ131_DEFAULT_LO_CONCENTRATION_R0;
            _timeToRead = MQ131_DEFAULT_LO_CONCENTRATION_TIMETOREAD;
            break;
        default: break;
    }
}

MQ131::~MQ131()
{}

byte MQ131::sendDataToServer()
{
    _instance.send("MQ131", "o3_concentration", "ppm", _value);
    return 0;
}

byte MQ131::setup()
{
    pinMode(_pinHeater, OUTPUT);
    pinMode(_pinValue, INPUT);
    digitalWrite(_pinHeater, LOW);
    calibrate();
    return 0;
}

byte MQ131::compute()
{
    if (_start == false) {
        digitalWrite(_pinHeater, HIGH);
        _startMillis = millis() / 1000;
        _start = true;
    }

    if (millis() / 1000 >= _startMillis + _timeToRead) {
        computeRs();
        computeIntoPPM();

        _startMillis = 0;
        _start = false;
        digitalWrite(_pinHeater, LOW);
    }
    return 0;
}

byte MQ131::compute(int temperature, int humidity)
{
    _temperature = temperature;
    _humidity = humidity;
    return compute();
}

float MQ131::getO3() const
{
    return _value;
}

void MQ131::computeIntoPPM()
{
    float ratio = 0;

    switch (_model) {
        case LOW_CONCENTRATION:
            ratio = _value / _r0 * getEnvRatio();
            _value =  (9.4783 * pow(ratio, 2.3348)) / 1000;
            break;
        case HIGH_CONCENTRATION:
            ratio = _value / _r0 * getEnvRatio();
            _value =  (8.1399 * pow(ratio, 2.3297));
            break;
        case SN_O2_LOW_CONCENTRATION:
            ratio = 12.15 * _value / _r0 * getEnvRatio();
            _value = (26.941 * pow(ratio, -1.16)) / 1000;
            break;
    }
}

void MQ131::computeRs()
{
    _value = analogRead(_pinValue);
    float vRL = _value  / 1024.0 * 5.0f;

    if (!vRL) {
        _value = 0;
    } else {
        _value = (5.0f / vRL - 1.0) * _valueRL;
    }
}

float MQ131::getEnvRatio() {
    if (_humidity == 60 && _temperature == 20) {
        return 1.0;
 	}
 	if (_humidity > 75) {
 		return -0.0141 * _temperature + 1.5623;
 	}
 	if (_humidity > 50) {
 		return -0.0119 * _temperature + 1.3261;
 	}
 	return -0.0103 * _temperature + 1.1507;
}

void MQ131::calibrate()
{
    float tempRsValue = 0;
    float lastRsValue = 0;
    byte countReadInRow = 0;
    byte count = 0;

    digitalWrite(_pinHeater, HIGH);
    while (countReadInRow <= MQ131_DEFAULT_STABLE_CYCLE) {
        computeRs();
        if((uint32_t)lastRsValue != (uint32_t)_value && (uint32_t)tempRsValue != (uint32_t)_value) {
            tempRsValue = lastRsValue;
            lastRsValue = _value;
            countReadInRow = 0;
        } else {
            countReadInRow++;
        }
        ++count;
        delay(1000);
    }
    digitalWrite(_pinHeater, LOW);
    _r0 = lastRsValue;
    _timeToRead = count;
}