/**
 * @file PPD42NS.h
 * @author Paul Beauduc (@acerlorion)
 * @brief PPD42NS class definition
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PPD42NS_H_
#define PPD42NS_H_

#include "include/env.h"
#include "include/ArduinoJson.h"

#include "../ISensor.h"
#include "../ScanAirLib.h"

/** @class PPD42NS
 * @brief PPD42NS ScanAir class
 * 
 * This class encapsulate the behaviour of the PPD42NS sensor while following the life cycle determined in ISensor.h.
 * Inherit from ISensor.h.
 * 
 * @see ISensor.h
 */
class PPD42NS : public ISensor
{
    private:
        ScanAir &_instance;
        byte _pin;
        unsigned long _start;
        unsigned long _sampletime;
        unsigned long _duration;
        unsigned long _lowpulse = 0;
        unsigned long _lowpulse_value = 0;
        float _ratio = 0;
        float _concentration = 0;
    public:
        /**
         * @brief Construct a new PPD42NS object
         * 
         * @param instance ScanAir instance.
         * @param pin Arduino pin where the data can be read. Usually A0.
         * @param sampletime Time needed for each sample. Set to 30000 if not given.
         */
        PPD42NS(ScanAir &instance, byte pin, unsigned long sampletime = 30000);

        /**
         * @brief Destroy the PPD42NS object
         */
        ~PPD42NS() final;

        /**
         * @brief Construct a new PPD42NS object from another constant one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another PPD42NS constant object.
         */
        PPD42NS(const PPD42NS &other) = default;

        /**
         * @brief Construct a new PPD42NS object from another one
         * 
         * Default C++11 behaviour.
         * 
         * @param other another PPD42NS object.
         */
        PPD42NS(PPD42NS &&other) = default;

        PPD42NS &operator=(const PPD42NS &other) = default;
        PPD42NS &operator=(PPD42NS &&other) = delete;

        /**
         * @brief PPD42NS sendDataToServer function
         * 
         * This function send to the ScanAir's API the dust concentration.
         * 
         * @see ISensor::sendDataToServer()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte sendDataToServer() final;

        /**
         * @brief PPD42NS setup function
         * 
         * This function setup the sensor and its pin.
         * 
         * @see ISensor::setup()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte setup() final;

        /**
         * @brief PPD42NS compute function
         * 
         * This function compute the dust concentration from the PPD42NS voltage and resistance.
         * 
         * @see ISensor::compute()
         * 
         * @return 1 if bad, 0 if good as byte.
         */
        byte compute() final;

        /**
         * @brief Get the dust concentration in pcs/L
         * 
         * @return the dust concentration as float 
         */
        float getDustConcentration() const;

        /**
         * @brief Get the level the low pulse occupancy (LPO) time takes up the whole sample time.
         * 
         * @return the ratio as float 
         */
        float getRatio() const;

        /**
         * @brief Get the low pulse occupancy (LPO) 
         * 
         * It represents the time needed by the sensor to go in LOW state. Its unit is microseconds.
         * 
         * @return the low pulse occupancy time as unsigned long 
         */
        unsigned long getLowPulseOccupancy() const;
};

#endif /* PPD42NS_H_ */
