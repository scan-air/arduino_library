/**
 * @file PPD42NS.cpp
 * @author Paul Beauduc (@acerlorion)
 * @brief PPD42NS class implementation
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright MIT License Copyright (c) 2020 Scan'Air
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "PPD42NS.h"

PPD42NS::PPD42NS(ScanAir &instance, byte pin, unsigned long sampletime)
    : _instance(instance), _pin(pin), _sampletime(sampletime)
{}

PPD42NS::~PPD42NS()
{}

byte PPD42NS::sendDataToServer()
{
    _instance.send("PPD42NS", "dust_concentration", "pcs/L", _concentration);
    return 0;
}

byte PPD42NS::setup()
{
    pinMode(_pin, INPUT);
    _start = millis();
    return 0;
}

byte PPD42NS::compute()
{
    _duration = pulseIn(_pin, LOW);
    _lowpulse = _lowpulse + _duration;

    if ((millis() - _start) > _sampletime) {
        _ratio = _lowpulse / ( _sampletime * 10.0f );
        _concentration = 1.1f * pow(_ratio, 3.0f) - 3.8f * pow(_ratio, 2.0f) + 520 * _ratio + 0.62f;
        _lowpulse_value = _lowpulse;
        _lowpulse = 0;
        _start = millis();
    }
    return 0;
}

float PPD42NS::getDustConcentration() const
{
    return _concentration;
}

float PPD42NS::getRatio() const
{
    return _ratio;
}

unsigned long PPD42NS::getLowPulseOccupancy() const
{
    return _lowpulse_value;
}
